package cn.zzm.zabbix.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cn.zzm.zabbix.BaseTestCase;
import cn.zzm.zabbix.model.Group;

public class HostDaoTest extends BaseTestCase{
	
	@Autowired
	private HostDao hostDao;
	
	@Test
	public void test() {
		
		assertNotNull(hostDao);
		for(Group g : hostDao.getOne("10105").getGroups()){
			System.out.println(g.getName());
		}
	}

}
