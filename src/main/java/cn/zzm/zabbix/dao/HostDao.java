package cn.zzm.zabbix.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.zzm.zabbix.model.Host;

public interface HostDao extends JpaRepository<Host, String>{
}
