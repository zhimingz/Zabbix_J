package cn.zzm.zabbix.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.zzm.zabbix.model.Group;


public interface GroupDao extends JpaRepository<Group, String>{
}
