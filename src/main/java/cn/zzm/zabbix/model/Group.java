package cn.zzm.zabbix.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the groups database table.
 * 
 */
@Entity(name="groups")
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;
	private String groupid;
	private int flags;
	private int internal;
	private String name;
	private List<Host> hosts;

	public Group() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	public String getGroupid() {
		return this.groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}


	public int getFlags() {
		return this.flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}


	public int getInternal() {
		return this.internal;
	}

	public void setInternal(int internal) {
		this.internal = internal;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	//bi-directional many-to-many association to Host
	@ManyToMany(mappedBy="groups", fetch=FetchType.LAZY)
	public List<Host> getHosts() {
		return this.hosts;
	}

	public void setHosts(List<Host> hosts) {
		this.hosts = hosts;
	}

}