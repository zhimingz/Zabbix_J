package cn.zzm.zabbix.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the hosts database table.
 * 
 */
@Entity(name="hosts")
public class Host implements Serializable {
	private static final long serialVersionUID = 1L;
	private String hostid;
	private int available;
	private String description;
	private int disableUntil;
	private String error;
	private int errorsFrom;
	private int flags;
	private String host;
	private int ipmiAuthtype;
	private int ipmiAvailable;
	private int ipmiDisableUntil;
	private String ipmiError;
	private int ipmiErrorsFrom;
	private String ipmiPassword;
	private int ipmiPrivilege;
	private String ipmiUsername;
	private int jmxAvailable;
	private int jmxDisableUntil;
	private String jmxError;
	private int jmxErrorsFrom;
	private int lastaccess;
	private int maintenanceFrom;
	private int maintenanceStatus;
	private int maintenanceType;
	private java.math.BigInteger maintenanceid;
	private String name;
	private java.math.BigInteger proxyHostid;
	private int snmpAvailable;
	private int snmpDisableUntil;
	private String snmpError;
	private int snmpErrorsFrom;
	private int status;
	private java.math.BigInteger templateid;
	private List<Group> groups;

	public Host() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	public String getHostid() {
		return this.hostid;
	}

	public void setHostid(String hostid) {
		this.hostid = hostid;
	}


	public int getAvailable() {
		return this.available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}


	@Lob
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Column(name="disable_until")
	public int getDisableUntil() {
		return this.disableUntil;
	}

	public void setDisableUntil(int disableUntil) {
		this.disableUntil = disableUntil;
	}


	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}


	@Column(name="errors_from")
	public int getErrorsFrom() {
		return this.errorsFrom;
	}

	public void setErrorsFrom(int errorsFrom) {
		this.errorsFrom = errorsFrom;
	}


	public int getFlags() {
		return this.flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}


	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}


	@Column(name="ipmi_authtype")
	public int getIpmiAuthtype() {
		return this.ipmiAuthtype;
	}

	public void setIpmiAuthtype(int ipmiAuthtype) {
		this.ipmiAuthtype = ipmiAuthtype;
	}


	@Column(name="ipmi_available")
	public int getIpmiAvailable() {
		return this.ipmiAvailable;
	}

	public void setIpmiAvailable(int ipmiAvailable) {
		this.ipmiAvailable = ipmiAvailable;
	}


	@Column(name="ipmi_disable_until")
	public int getIpmiDisableUntil() {
		return this.ipmiDisableUntil;
	}

	public void setIpmiDisableUntil(int ipmiDisableUntil) {
		this.ipmiDisableUntil = ipmiDisableUntil;
	}


	@Column(name="ipmi_error")
	public String getIpmiError() {
		return this.ipmiError;
	}

	public void setIpmiError(String ipmiError) {
		this.ipmiError = ipmiError;
	}


	@Column(name="ipmi_errors_from")
	public int getIpmiErrorsFrom() {
		return this.ipmiErrorsFrom;
	}

	public void setIpmiErrorsFrom(int ipmiErrorsFrom) {
		this.ipmiErrorsFrom = ipmiErrorsFrom;
	}


	@Column(name="ipmi_password")
	public String getIpmiPassword() {
		return this.ipmiPassword;
	}

	public void setIpmiPassword(String ipmiPassword) {
		this.ipmiPassword = ipmiPassword;
	}


	@Column(name="ipmi_privilege")
	public int getIpmiPrivilege() {
		return this.ipmiPrivilege;
	}

	public void setIpmiPrivilege(int ipmiPrivilege) {
		this.ipmiPrivilege = ipmiPrivilege;
	}


	@Column(name="ipmi_username")
	public String getIpmiUsername() {
		return this.ipmiUsername;
	}

	public void setIpmiUsername(String ipmiUsername) {
		this.ipmiUsername = ipmiUsername;
	}


	@Column(name="jmx_available")
	public int getJmxAvailable() {
		return this.jmxAvailable;
	}

	public void setJmxAvailable(int jmxAvailable) {
		this.jmxAvailable = jmxAvailable;
	}


	@Column(name="jmx_disable_until")
	public int getJmxDisableUntil() {
		return this.jmxDisableUntil;
	}

	public void setJmxDisableUntil(int jmxDisableUntil) {
		this.jmxDisableUntil = jmxDisableUntil;
	}


	@Column(name="jmx_error")
	public String getJmxError() {
		return this.jmxError;
	}

	public void setJmxError(String jmxError) {
		this.jmxError = jmxError;
	}


	@Column(name="jmx_errors_from")
	public int getJmxErrorsFrom() {
		return this.jmxErrorsFrom;
	}

	public void setJmxErrorsFrom(int jmxErrorsFrom) {
		this.jmxErrorsFrom = jmxErrorsFrom;
	}


	public int getLastaccess() {
		return this.lastaccess;
	}

	public void setLastaccess(int lastaccess) {
		this.lastaccess = lastaccess;
	}


	@Column(name="maintenance_from")
	public int getMaintenanceFrom() {
		return this.maintenanceFrom;
	}

	public void setMaintenanceFrom(int maintenanceFrom) {
		this.maintenanceFrom = maintenanceFrom;
	}


	@Column(name="maintenance_status")
	public int getMaintenanceStatus() {
		return this.maintenanceStatus;
	}

	public void setMaintenanceStatus(int maintenanceStatus) {
		this.maintenanceStatus = maintenanceStatus;
	}


	@Column(name="maintenance_type")
	public int getMaintenanceType() {
		return this.maintenanceType;
	}

	public void setMaintenanceType(int maintenanceType) {
		this.maintenanceType = maintenanceType;
	}


	public java.math.BigInteger getMaintenanceid() {
		return this.maintenanceid;
	}

	public void setMaintenanceid(java.math.BigInteger maintenanceid) {
		this.maintenanceid = maintenanceid;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Column(name="proxy_hostid")
	public java.math.BigInteger getProxyHostid() {
		return this.proxyHostid;
	}

	public void setProxyHostid(java.math.BigInteger proxyHostid) {
		this.proxyHostid = proxyHostid;
	}


	@Column(name="snmp_available")
	public int getSnmpAvailable() {
		return this.snmpAvailable;
	}

	public void setSnmpAvailable(int snmpAvailable) {
		this.snmpAvailable = snmpAvailable;
	}


	@Column(name="snmp_disable_until")
	public int getSnmpDisableUntil() {
		return this.snmpDisableUntil;
	}

	public void setSnmpDisableUntil(int snmpDisableUntil) {
		this.snmpDisableUntil = snmpDisableUntil;
	}


	@Column(name="snmp_error")
	public String getSnmpError() {
		return this.snmpError;
	}

	public void setSnmpError(String snmpError) {
		this.snmpError = snmpError;
	}


	@Column(name="snmp_errors_from")
	public int getSnmpErrorsFrom() {
		return this.snmpErrorsFrom;
	}

	public void setSnmpErrorsFrom(int snmpErrorsFrom) {
		this.snmpErrorsFrom = snmpErrorsFrom;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public java.math.BigInteger getTemplateid() {
		return this.templateid;
	}

	public void setTemplateid(java.math.BigInteger templateid) {
		this.templateid = templateid;
	}


	//bi-directional many-to-many association to Group
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="HOSTS_GROUPS",joinColumns={@JoinColumn(name="hostid")})
	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

}